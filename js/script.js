$(function() {
    $('.link').click( function(e) {
        if ($(this).hasClass('menuparent')) {
            $parent = $(this).parent();
            // Sinon le supprime du menu
            $parent.siblings().removeClass('open');

            // Ajout la classe "active" sur le lien selectionné
            if ($parent.hasClass('open')) {
                $parent.removeClass('open');
            } else {
                $parent.addClass('open');
            }
        } else {
            $('.item').removeClass('open');
        }

        // Empeche le lien d'emmener sur une autre page
        e.preventDefault();
    });

    $('.button-hamburger').click( function() {
        $('.menu.depth-0').toggle();
    });

    $('a.gallery').colorbox({rel:'gal'});
    $('.slider-carrousel').slick({
        arrows: true,
        dots: true
    });
    $('.slider-main').slick({
        fade: true,
        arrows: false,
        asNavFor: 'slider-navigation'
    });
    $('.slider-navigation').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-main',
        dots: true,
        centerMode: true,
        focusOnSelect: true
    });
});
